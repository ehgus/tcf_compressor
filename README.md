# TCF compressor

Compresse TCF (TomoCube File) data into more compressed one. It compresses data that are still compatible with [Fiji/ImageJ2](https://imagej.net/software/fiji/) and [napari](https://napari.org/).


## Usage
```Shell
python compress_tcf.py [-h] [--overwrite OVERWRITE] filename
```

where "filename" indicate the TCF file to be compressed.

The output file will be named with prefix "compressed_" when overwriting feature is not specified.

## What is TCF data? / How to read it? 
TCF data is an custom HDF5 file for saving holotomographic data. That is, the file can be opend with HDF5 viewer. In ImageJ, you can open it with 'File > Import > HDF5'. For napari, you may use h5 plugin or [Tomocube data viewer](https://www.napari-hub.org/plugins/napari-tomocube-data-viewer).
